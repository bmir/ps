Rails.application.routes.draw do
  root to: 'messages#index'

  resources :messages do
    collection do
      get :about
      post :search
      get :new_search
    end
  end
end
