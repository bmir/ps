module ApplicationHelper
  def active_link?(path_url)
    'active' if current_page?(path_url)
  end

  def prettify_date(date_str)
    return if date_str.blank?

    DateTime.parse(date_str).strftime("%m/%d/%Y (%I:%M:%S %p)")
  end
end
