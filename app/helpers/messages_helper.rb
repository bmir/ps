module MessagesHelper
  def format_transmission_info(transmission)
    return I18n.t('no_data') if transmission.blank?

    transmission['status']
  end
end
