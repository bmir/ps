class MessagesController < ApplicationController
  include Messenger::Api::Client::Messages

  MAX_MESSAGES = 100

  def index
    @messages = retrieve_all(0, MAX_MESSAGES)
  end

  def create
    destination = params[:destination]
    content = params[:content]

    @send_response = send_message(destination, content)
    if @send_response['success'] == "false"
      flash.now[:error] = @send_response['errors']
      render :new
    else
      flash[:notice] = I18n.t('message_sent')
      redirect_to messages_path
    end
  end

  def about
  end

  def show
    @message = retrieve(params[:id])
  end

  def new_search
  end

  def search
    destination = params[:destination]

    if destination.blank?
      flash.now[:error] = I18n.t('destination_blank')
      render :new_search
    else
      @messages = retrieve_by_destination(destination, nil, MAX_MESSAGES)
      @search = true

      render :index
    end
  end
end
